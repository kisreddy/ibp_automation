﻿'Load Framework functions
ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"
StartTime=Now()

'Read output of Verification_Dataload_Checks_Baselines_Report script log for job run status pass or fail
strJobCompStatus=fn_Read_Values_From_File("Verification_Dataload_Checks_Baselines_Report")

If lcase(strJobCompStatus)="fail" or lcase(strJobCompStatus)<>"pass" Then
	reporter.ReportEvent micFail,"Job run status","Job run status is failed or not available"
	call fn_Write_Summary_Report("Fail","Job run status","Job run status is failed or not available","")
	Call fn_Send_Results_Email(StartTime)
	exitrun
End If

'Open IBP excel
Call fn_Open_IBP_Excel(fn_getdatatablevalue("IBP_Addin_Connection_Name"))

'Open IBP report From MDT favorites
Call fn_Open_Excel_IBP_Report_MDT(fn_getdatatablevalue("Excel_Report_Name"))


'Compare offset tabs
Call fn_Compare_ExcelSheets("A2","Offset_BS;Offset_ASML")

'COMPARE OBJ SHEETS

Call fn_Compare_ExcelSheets("A2","Obj_BS;Obj_ASML")

'Compare OpNr tabs
Call fn_Compare_OpNr_EP_OpNrCP_OpNrBS_ExcelSheets()

'Compare Testrig Tabs
Call fn_Compare_Testrig_Tabs("A2","L.Testrig_ASML;P.L.Testrig_ASML")

Call fn_Send_Results_Email(StartTime)



