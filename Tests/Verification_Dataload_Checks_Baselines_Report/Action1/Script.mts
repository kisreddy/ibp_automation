﻿On error resume next

Dim boolResult,strStatus,Script_Excution_Start_Time
'Load Framework functions
ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"
 
 'SAP ECC job running
 boolResult=SAP_ECC_JobStatus()
 
 If boolResult=False Then
 	exitrun
 End If
 
'Web login
call fn_IBP_Web_Login

'Schedule the Job
Call fn_IBP_Scheduling_job(fn_getdatatablevalue("Job_Name"))

'Search Job for status 
strStatus = fn_IBP_JobSerach_Status(fn_getdatatablevalue("Job_Name"))

'Verification of SAP job status
if strStatus <> "Finished" then
Reporter.ReportEvent micFail, "Not validating IBP Job" ,"Due to Job status is Failed in schedule"
call fn_Write_Summary_Report("Fail","Not validating IBP Job" ,"Due to Job status is Failed in schedule","")
exitrun
End If

Reporter.ReportEvent micPass,"Job passed","Job Passed"

Call fn_Open_IBP_Excel(fn_getdatatablevalue("IBP_Addin_Connection_Name"))

'Open Excel Report from Favorites
Call Open_Excel_IBP_Report(fn_getdatatablevalue("Excel_Report_Name"))

Script_Excution_Start_Time = Date()&":"&Now()

'Verification of Different tabs of the report
Call Verify_Dataload_checks_Baseline_Holiday_Tab()

Call Verify_Dataload_checks_Baseline_AKF_Tab()

Call Verify_Dataload_checks_Baseline_APR_eq_PXX() 

Call Verify_Dataload_checks_Baseline_PWO_notEq_pxx()

Call Verify_Dataload_checks_Baseline_PWO_Eq_pxx()

Call Verify_Dataload_checks_Baseline_CPI_KF()


'Send report to email
Call fn_Send_Results_Email(Script_Excution_Start_Time)



