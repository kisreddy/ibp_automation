GBL_Testspathvalue="C:\IBP_Automation\Tests\"

Set oExcel = CreateObject("Excel.Application")
oExcel.DisplayAlerts = False
Set oMasterDriverWorkbook = oExcel.Workbooks.Open("C:\IBP_Automation\BatchRunner\TestBatchRunner_Excel.xlsx")
Set oMasterDriverSheet = oMasterDriverWorkbook.Worksheets("TestSet")
For i = 2 To oMasterDriverSheet.usedrange.rows.count Step 1
	If oMasterDriverSheet.cells(i,2)="Y" Then
		strTestlist=strTestlist&oMasterDriverSheet.cells(i,1)&";"
	End If
Next 
strTestlist=left(strTestlist,len(strTestlist)-1)

arrTestlist=split(strTestlist,";")

Set objQTP = CreateObject("QuickTest.Application")

ObjQTP.Launch
objQTP.Visible=True
Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")

'qtResultsOpt.ResultsLocation ="C:\IBP_Automation\Results"& Environment("TestName")
For each tcname in arrTestlist
	ObjQTP.open GBL_Testspathvalue&tcname
	qtResultsOpt.ResultsLocation ="C:\IBP_Automation\Results\"& tcname&"_"&replace(replace(replace(now,"/","_"),":","_")," ","_")
	ObjQTP.Test.Run qtResultsOpt,True
	'ObjQTP.Test.Save
	ObjQTP.Test.Close
Next

Set qtResultsOpt=Nothing

ObjQTP.Quit

Set objQTP=Nothing
